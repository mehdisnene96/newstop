import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { Mutation } from 'react-apollo';
import { Spacing } from 'components/Layout';
import { H1, Error } from 'components/Text';
import { InputText, Button } from 'components/Form';
import Head from 'components/Head';
import { SIGN_UP } from 'graphql/user';
import { withRouter } from 'react-router-dom';

import * as Routes from 'routes';

const Root = styled.div`
  padding: 0 ${p => p.theme.spacing.sm};
`;

const Container = styled.div`
  width: 450px;
  margin: 0 auto;
  background-color: ${p => p.theme.colors.white};
  padding: ${p => p.theme.spacing.md};
  border-radius: ${p => p.theme.radius.sm};
  width: 100%;
  margin-top: 80px;

  @media (min-width: ${p => p.theme.screen.sm}) {
    width: 450px;
  }

  @media (min-width: ${p => p.theme.screen.md}) {
    margin-top: auto;
  }
`;



/**
 * Forgot password page
 */
const CompanyAccount = ({ history }) =>  {
  const [error, setError] = useState('');
  const [values, setValues] = useState({
    fullName: '',
    username: '',
    email: '',
    password: '',
  });

  const handleChange = e => {
    const { name, value } = e.target;
    setValues({ ...values, [name]: value });
  };

  const validate = () => {
    if (!fullName || !email || !username || !password) {
      return 'All fields are required';
    }

    if (fullName.length > 50) {
      return 'Full name no more than 50 characters';
    }

    const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!emailRegex.test(String(email).toLowerCase())) {
      return 'Enter a valid email address.';
    }

    const usernameRegex = /^(?!.*\.\.)(?!.*\.$)[^\W][\w.]{0,29}$/;
    if (!usernameRegex.test(username)) {
      return 'Usernames can only use letters, numbers, underscores and periods';
    } else if (username.length > 20) {
      return 'Username no more than 50 characters';
    }

    if (password.length < 6) {
      return 'Password min 6 characters';
    }

    return false;
  };

  const handleSubmit = (e, signup) => {
    e.preventDefault();

    const error = validate();
    if (error) {
      setError(error);
      return false;
    }

    signup().then(async ({ data }) => {
      localStorage.setItem('token', data.signup.token);
      
      history.push(Routes.HOME);
    });
  };

  const renderErrors = apiError => {
    let errorMessage;

    if (error) {
      errorMessage = error;
    } else if (apiError) {
      errorMessage = apiError.graphQLErrors[0].message;
    }

    if (errorMessage) {
      return (
        <Spacing bottom="sm" top="sm">
          <Error>{errorMessage}</Error>
        </Spacing>
      );
    }

    return null;
  };

  const { fullName, email, password, username,company } = values;
  values.company=true;

  return (
    <Mutation
      mutation={SIGN_UP}
      variables={{ input: { fullName, email, password, username ,company} }}
    >
      {(signup, { loading, error: apiError }) => {
        return (
          <Root maxWidth="lg">
            <Head />
            <Container> 
            
              <Spacing bottom="md">
                <H1>Create Company Account</H1>
              </Spacing>

              <form onSubmit={e => handleSubmit(e, signup)}>
                <InputText
                  type="text"
                  name="fullName"
                  values={fullName}
                  onChange={handleChange}
                  placeholder="company name"
                  borderColor="white"
                />
                <Spacing top="xs" bottom="xs">
                  <InputText
                    type="text"
                    name="email"
                    values={email}
                    onChange={handleChange}
                    placeholder="Email"
                    borderColor="white"
                  />
                </Spacing>
                <InputText
                  type="text"
                  name="username"
                  values={username}
                  onChange={handleChange}
                  placeholder="Username"
                  borderColor="white"
                />
                <Spacing top="xs" bottom="xs">
                  <InputText
                    type="password"
                    name="password"
                    values={password}
                    onChange={handleChange}
                    placeholder="Password"
                    borderColor="white"
                  />
                </Spacing>
               
              
                {renderErrors(apiError)}

                <Spacing top="sm" />
                <Button size="large" disabled={loading}>
                  Sign up
                </Button>
              </form>
            
            </Container>
            </Root>
          
        );
      }}
    </Mutation>
  );
};

CompanyAccount.propTypes = {
  history: PropTypes.object.isRequired,
  

};

export default withRouter(CompanyAccount);
